//
//  Task+CoreDataProperties.swift
//  Checklist
//
//  Created by Pavel Chupryna on 17/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//
//

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var isChecked: Bool
    @NSManaged public var title: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var list: List?

}
