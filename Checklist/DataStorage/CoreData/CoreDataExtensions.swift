//
//  CoreDataExtensions.swift
//  Checklist
//
//  Created by Pavel Chupryna on 16/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation
import CoreData

extension List {
    func toListItem() -> ListItemProtocol {
        let tasks: [TaskItemProtocol] = self.tasks?.map{ ($0 as! Task).toTaskItem() }.sorted{ $0.date > $1.date } ?? []
        return ListItem(title: self.title!, checklist: tasks, id: self.id!, date: Date(timeIntervalSince1970: self.date!.timeIntervalSince1970))
    }
}

extension ListItemProtocol {
    func addCoreDataList(inContext context: NSManagedObjectContext?) {
        let list = List(entity: List.entity(), insertInto: context)
        list.id = self.id
        list.date = NSDate(timeIntervalSince1970: self.date.timeIntervalSince1970)
        list.title = self.title
        for task in self.tasks {
            task.addCoreDataTask(forList: list, inContext: context)
        }
    }
}

extension Task {
    func toTaskItem() -> TaskItemProtocol {
        return TaskItem(title: self.title!, isChecked: self.isChecked, id: self.id!, date: Date(timeIntervalSince1970: self.date!.timeIntervalSince1970))
    }
}

extension TaskItemProtocol {
    func addCoreDataTask(forList list: List, inContext context: NSManagedObjectContext?) {
        let task = Task(entity: Task.entity(), insertInto: context)
        task.id = self.id
        task.date = NSDate(timeIntervalSince1970: self.date.timeIntervalSince1970)
        task.title = self.title
        task.isChecked = self.isChecked
        list.addToTasks(task)
    }
}
