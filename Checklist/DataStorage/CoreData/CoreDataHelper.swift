//
//  CoreDataHelper.swift
//  Checklist
//
//  Created by Pavel Chupryna on 16/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class CoreDataHelper {
    
    static var persistentContainer: NSPersistentContainer {
       return (UIApplication.shared.delegate as? AppDelegate)!.persistentContainer
    }
    
    static func saveChanges() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                fatalError("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    static func getLists() -> [ListItemProtocol] {
        let context = persistentContainer.viewContext
        var lists: [List] = []
        
        do {
            lists = try context.fetch(List.fetchRequest())
        } catch let error as NSError {
            fatalError("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return lists.map{ $0.toListItem() }.sorted{ $0.date > $1.date }
    }
    
    static func listAdded(item: ListItemProtocol) {
        let context = persistentContainer.viewContext
        item.addCoreDataList(inContext: context)
        saveChanges()
    }
    
    static func listChanged(item: ListItemProtocol) {
        let context = persistentContainer.viewContext
        
        do {
            let lists = try context.fetch(List.fetchRequest())
            let list = lists.first(where: { ($0 as! List).id == item.id }) as! List
            list.title = item.title
        } catch let error as NSError {
            fatalError("Could not change list. \(error), \(error.userInfo)")
        }
        
        saveChanges()
    }
    
    static func listDelited(id: UUID) {
        let context = persistentContainer.viewContext
        
        do {
            let lists = try context.fetch(List.fetchRequest())
            let list = lists.first(where: { ($0 as! List).id == id }) as! List
            context.delete(list)
        } catch let error as NSError {
            fatalError("Could not delete list. \(error), \(error.userInfo)")
        }
        
        saveChanges()
    }
    
    static func taskAdded(listId: UUID, item: TaskItemProtocol) {
        let context = persistentContainer.viewContext
        
        do {
            let lists = try context.fetch(List.fetchRequest())
            let list = lists.first(where: { ($0 as! List).id == listId }) as! List
            item.addCoreDataTask(forList: list, inContext: context)
        } catch let error as NSError {
            fatalError("Could not add task. \(error), \(error.userInfo)")
        }
        
        saveChanges()
    }
    
    static func taskChanged(item: TaskItemProtocol) {
        let context = persistentContainer.viewContext
        
        do {
            let tasks = try context.fetch(Task.fetchRequest())
            let task = tasks.first(where: { ($0 as! Task).id == item.id }) as! Task
            task.title = item.title
            task.isChecked = item.isChecked
        } catch let error as NSError {
            fatalError("Could not change task. \(error), \(error.userInfo)")
        }
        
        saveChanges()
    }
    
    static func taskDelited(id: UUID) {
        let context = persistentContainer.viewContext
        
        do {
            let tasks = try context.fetch(Task.fetchRequest())
            let task = tasks.first(where: { ($0 as! Task).id == id }) as! Task
            context.delete(task)
        } catch let error as NSError {
            fatalError("Could not delete task. \(error), \(error.userInfo)")
        }
        
        saveChanges()
    }
}
