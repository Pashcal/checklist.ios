//
//  AppSettings.swift
//  Checklist
//
//  Created by Pavel Chupryna on 09/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

class UserDefaultsHelper {
    
    static let simulatedToDoNumberKey = "simulatedToDoNumber"
    
    static func saveSimulatedToDoNumber(_ number: Int) {
        UserDefaults.standard.set(number, forKey: simulatedToDoNumberKey)
        UserDefaults.standard.synchronize()
    }
    
    static func getSimulatedToDoNumber() -> Int {
        return UserDefaults.standard.integer(forKey: simulatedToDoNumberKey)
    }
}
