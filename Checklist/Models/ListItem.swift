//
//  ListItem.swift
//  Checklist
//
//  Created by Pavel Chupryna on 15/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

protocol ListItemProtocol {
    var id: UUID { get }
    var date: Date { get }
    var title: String { get set }
    var tasks: [TaskItemProtocol] { get set }
}

class ListItem: NSObject, ListItemProtocol {
    
    let id: UUID
    let date: Date
    var title: String
    var tasks: [TaskItemProtocol]
    
    init(title: String, checklist: [TaskItemProtocol] = [], id: UUID = UUID(), date: Date = Date()) {
        self.id = id
        self.date = date
        self.title = title
        self.tasks = checklist
    }
}
