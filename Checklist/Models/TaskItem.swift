//
//  ChecklistItem.swift
//  Checklist
//
//  Created by Pavel Chupryna on 08/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import Foundation

protocol TaskItemProtocol {
    var id: UUID { get }
    var date: Date { get }
    var title: String { get set }
    var isChecked: Bool { get set }
}

class TaskItem: NSObject, TaskItemProtocol {
    
    let id: UUID
    let date: Date
    var title: String
    var isChecked: Bool
    
    init(title: String, isChecked: Bool = false, id: UUID = UUID(), date: Date = Date()) {
        self.id = id
        self.date = date
        self.title = title
        self.isChecked = isChecked
    }
}
