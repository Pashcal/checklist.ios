//
//  AddItemViewController.swift
//  Checklist
//
//  Created by Pavel Chupryna on 08/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

struct SegueType {
    static let addItem = "AddItem"
    static let editItem = "EditItem"
}

protocol TaskItemDetailsDelegate: class {
    func itemAdded(_ item: TaskItemProtocol, by sender: ItemDetailsViewController)
    func itemEdited(_ item: TaskItemProtocol, by sender: ItemDetailsViewController)
}

protocol ListItemDetailsDelegate: class {
    func itemAdded(_ item: ListItemProtocol, by sender: ItemDetailsViewController)
    func itemEdited(_ item: ListItemProtocol, by sender: ItemDetailsViewController)
}

class ItemDetailsViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var nameTextField: UITextField!
    
    var taskItem: TaskItemProtocol?
    weak var taskDelegate: TaskItemDetailsDelegate?
    
    var listItem: ListItemProtocol?
    weak var listDelegate: ListItemDetailsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let editItem = taskItem {
            nameTextField.text = editItem.title
        } else if let editItem = listItem {
            nameTextField.text = editItem.title
        }
        
        nameTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nameTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        setEditing(false, animated: false)
    }
    
    @IBAction func nameTextFieldChanged(_ sender: Any) {
        doneButton.isEnabled = !(nameTextField.text?.isEmpty ?? false)
        if let editItem = taskItem {
            doneButton.isEnabled = !editItem.title.elementsEqual(nameTextField.text!) && doneButton.isEnabled
        } else if let editItem = listItem {
            doneButton.isEnabled = !editItem.title.elementsEqual(nameTextField.text!) && doneButton.isEnabled
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return doneButton.isEnabled
    }
    
    @IBAction func cancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done(_ sender: Any) {
        if let delegate = taskDelegate {
            if var editItem = taskItem {
                editItem.title = nameTextField.text!
                delegate.itemEdited(editItem, by: self)
            } else {
                delegate.itemAdded(TaskItem(title: nameTextField.text!), by: self)
            }
        } else if let delegate = listDelegate {
            if var editItem = listItem {
                editItem.title = nameTextField.text!
                delegate.itemEdited(editItem, by: self)
            } else {
                delegate.itemAdded(ListItem(title: nameTextField.text!), by: self)
            }
        }
        
        navigationController?.popViewController(animated: true)
    }
}
