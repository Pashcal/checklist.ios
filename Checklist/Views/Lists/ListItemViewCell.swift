//
//  ListItemViewCell.swift
//  Checklist
//
//  Created by Pavel Chupryna on 15/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class ListItemViewCell: UITableViewCell {
    
    static let key = "ListItem"
    static let nib = UINib(nibName: "ListItem", bundle: nil)
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
