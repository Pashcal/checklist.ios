//
//  ListViewController.swift
//  Checklist
//
//  Created by Pavel Chupryna on 15/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class ListsViewController: UITableViewController, ListItemDetailsDelegate {
    var emptyLabel: UILabel = UILabel()
    var items: [ListItemProtocol] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initEmptyView()
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        items = CoreDataHelper.getLists()
        tableView.reloadData()
    }
    
    func initEmptyView() {
        emptyLabel.numberOfLines = 0
        emptyLabel.textAlignment = .center
        emptyLabel.text = "To add a new TO-DO list,\npress the plus button at the top ↑"
        emptyLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
        
        view.addSubview(emptyLabel)
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        emptyLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emptyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 64).isActive = true
    }
    
    
    func itemAdded(_ item: ListItemProtocol, by sender: ItemDetailsViewController) {
        items.insert(item, at: 0)
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
        CoreDataHelper.listAdded(item: item)
    }
    
    func itemEdited(_ item: ListItemProtocol, by sender: ItemDetailsViewController) {
        if let itemIndex = items.index(where: { $0.id == item.id }) {
            tableView.reloadRows(at: [IndexPath(row: itemIndex, section: 0)], with: .fade)
            CoreDataHelper.listChanged(item: item)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            CoreDataHelper.listDelited(id: items[indexPath.row].id)
            items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (emptyLabel.alpha > 0) != (items.count == 0) {
            UIView.animate(withDuration: 0.2) { self.emptyLabel.alpha = (self.items.count == 0 ? 1 : 0) }
        }
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListItemViewCell.key, for: indexPath) as! ListItemViewCell
        
        let listItem = items[indexPath.row]
        cell.titleLabel.text = listItem.title
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Checklist" {
            let controller = segue.destination as! TasksViewController
            if let cellIndexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                let listItem = items[cellIndexPath.row]
                controller.navigationItem.title = listItem.title
                controller.listId = listItem.id
                controller.items = listItem.tasks
            }
        } else if segue.identifier == SegueType.addItem || segue.identifier == SegueType.editItem {
            let controller = segue.destination as! ItemDetailsViewController
            controller.navigationItem.title = "Add list"
            controller.listDelegate = self
            if segue.identifier == SegueType.editItem {
                controller.navigationItem.title = "Edit list"
                if let cellIndexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                    controller.listItem = items[cellIndexPath.row]
                }
            }
        }
    }
}
