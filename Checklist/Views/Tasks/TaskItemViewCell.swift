//
//  ChecklistItemViewCell.swift
//  Checklist
//
//  Created by Pavel Chupryna on 10/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class TaskItemViewCell: UITableViewCell {
    
    static let key = "TaskItem"
    static let nib = UINib(nibName: "TaskItem", bundle: nil)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkmarkBorderView: UIView!
    @IBOutlet weak var checkmarkView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        checkmarkView.layer.cornerRadius = checkmarkView.bounds.height / 2
        checkmarkBorderView.layer.cornerRadius = checkmarkBorderView.bounds.height / 2
        checkmarkBorderView.layer.borderWidth = 1
        checkmarkBorderView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func setCheckmark(_ isCheked: Bool) {
        UIView.animate(withDuration: 0.2) { self.checkmarkView.alpha = isCheked ? 1 : 0 }
    }

}
