//
//  ViewController.swift
//  Checklist
//
//  Created by Pavel Chupryna on 08/03/2018.
//  Copyright © 2018 pashcal. All rights reserved.
//

import UIKit

class TasksViewController: UITableViewController, TaskItemDetailsDelegate {
    var emptyLabel: UILabel = UILabel()
    var listId: UUID?
    var items: [TaskItemProtocol] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initEmptyView()
        tableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
    }
    
    func initEmptyView() {
        emptyLabel.numberOfLines = 0
        emptyLabel.textAlignment = .center
        emptyLabel.text = "To add a new TO-DO,\npress the plus button at the top ↑"
        emptyLabel.font = UIFont.systemFont(ofSize: 20, weight: .light)
        
        view.addSubview(emptyLabel)
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        emptyLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emptyLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 64).isActive = true
    }
    
    @objc func pullToRefresh(_ sender: Any) {
        DispatchQueue(label: "simulated_remote_connection").async {
            sleep(2)
            DispatchQueue.main.async {
                let simulatedToDoPrefix = "Simulated remote to-do"
                let nextSimulatedNumber = UserDefaultsHelper.getSimulatedToDoNumber() + 1
                
                let task = TaskItem(title: "\(simulatedToDoPrefix) #\(nextSimulatedNumber)")
                self.items.insert(task, at: 0)
                self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .left)
                
                UserDefaultsHelper.saveSimulatedToDoNumber(nextSimulatedNumber)
                CoreDataHelper.taskAdded(listId: self.listId!, item: task)
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    
    func itemAdded(_ item: TaskItemProtocol, by sender: ItemDetailsViewController) {
        items.insert(item, at: 0)
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
        CoreDataHelper.taskAdded(listId: listId!, item: item)
    }
    
    func itemEdited(_ item: TaskItemProtocol, by sender: ItemDetailsViewController) {
        if let itemIndex = items.index(where: { $0.id == item.id }) {
            tableView.reloadRows(at: [IndexPath(row: itemIndex, section: 0)], with: .fade)
            CoreDataHelper.taskChanged(item: item)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            CoreDataHelper.taskDelited(id: items[indexPath.row].id)
            items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (emptyLabel.alpha > 0) != (items.count == 0) {
            UIView.animate(withDuration: 0.2) { self.emptyLabel.alpha = (self.items.count == 0 ? 1 : 0) }
        }
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TaskItemViewCell.key, for: indexPath) as! TaskItemViewCell
        
        let taskItem = items[indexPath.row]
        cell.titleLabel.text = taskItem.title
        cell.setCheckmark(taskItem.isChecked)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? TaskItemViewCell {
            var taskItem = items[indexPath.row]
            taskItem.isChecked = !taskItem.isChecked
            cell.setCheckmark(taskItem.isChecked)
            CoreDataHelper.taskChanged(item: taskItem)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueType.addItem || segue.identifier == SegueType.editItem {
            let controller = segue.destination as! ItemDetailsViewController
            controller.navigationItem.title = "Add to-do"
            controller.taskDelegate = self
            if segue.identifier == SegueType.editItem {
                controller.navigationItem.title = "Edit to-do"
                if let cellIndexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                    controller.taskItem = items[cellIndexPath.row]
                }
            }
        }
    }
}

