# Checklist.iOS

#### Very simple checklist app with local data saving

Created for testing native iOS development using Swift

Includes:

* Storyboards for navigation and creating every layouts
* UserDefaults for saving data in json
